const express = require('express')
const http = require('http');
const cors = require('cors')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const os = require('os')

//Init express
const app = express()
const port = '3000'
app.set('port', port)

//Enable bodyParser
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:false}))
 
//Enable CORS
app.use(cors())

// Router
var router = express.Router()
app.use(router)

// MongoDB
const options = {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      autoIndex: false, // Don't build indexes
      reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
      reconnectInterval: 500, // Reconnect every 500ms
      poolSize: 10, // Maintain up to 10 socket connections
      // If not connected, return errors immediately rather than waiting for reconnect
      bufferMaxEntries: 0,
      connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
      socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
      family: 4 // Use IPv4, skip trying IPv6
    };

var lastReconnectAttempt = new Date().getTime();
const urlmongo = "mongodb://testapp-db:27017/vcn"; 
mongoose.connect(urlmongo, options);
var db = mongoose.connection; 
db.on('error', console.error.bind(console, 'Erreur lors de la connexion'))
db.once('open', function (){
    console.log("Connexion à la base OK")
})
db.on('disconnected', function() {
      console.log('MongoDB disconnected!');
      var now = new Date().getTime();
      // check if the last reconnection attempt was too early
      if (lastReconnectAttempt && now-lastReconnectAttempt<5000) {
          // if it does, delay the next attempt
          var delay = 5000-(now-lastReconnectAttempt);
          console.log('reconnecting to MongoDB in ' + delay + "mills");
          setTimeout(function() {
              console.log('reconnecting to MongoDB');
              lastReconnectAttempt=new Date().getTime();
              mongoose.connect(urlmongo, options);
          },delay);
      }
      else {
          console.log('reconnecting to MongoDB');
          lastReconnectAttempt=now;
          mongoose.connect(urlmongo, options);
      }
  
  });

const VoteSchema = new mongoose.Schema({
      entity: {
            type: String,
            required: true
      },
      votes: {
            type: Number,
            required: true
      },
      ip: {
            type: String,
            required: false
      }})


var VoteModel = mongoose.model('Vote', VoteSchema)

var r = {
      votes: VoteSchema,
      metadata: {
            hostname: String,
            platform: String,
            arch: String,
            release: String,
            version: String
      }
};

router.route('/api/vote')
.post(function(req,res){
      console.log("post")
      
      VoteModel.findOneAndUpdate({entity:req.body.entity}, { $inc: { votes: +1}, ip: os.hostname() }, {upsert: true}, function (err, doc) {
            if (err) { console.log(err); res.sendStatus(500) }
            else {
                  //console.log(doc); 
                  res.json('ok')    
            }
      });
})

.get(function(req,res){
      console.log("get")
      
      VoteModel.find(function (err, doc) {
            if (err) {
                   console.log(err)
                   res.sendStatus(500)
                  }
            else {
                  //doc.type = os.type();
                  //doc.plateform = os.platform();
                  //doc.arch = os.arch();
                  //doc.release = os.release();
                  r.votes = doc;
                  r.metadata.hostname = os.hostname();
                  r.metadata.platform = os.platform();
                  r.metadata.arch = os.arch();
                  r.metadata.release = os.release();
                  r.metadata.version = process.env.VERSION || "v1"

                  console.log(r); 
                  res.json(r)   
            }
      });
})

.delete(function(req,res){
      console.log("delete")
      
      VoteModel.remove({}, function (err, doc) {
            if (err) {
                   console.log(err)
                   res.sendStatus(500)
                  }
            else {
                  console.log(doc);  
                  res.json(doc)   
            }
      });
})


//Create the API server.
const server = http.createServer(app);
server.listen(port, () => console.log(`API running on localhost:${port}`))

